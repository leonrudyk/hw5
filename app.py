from flask import Flask, request
import requests
from faker import Faker

from db import execute_query
from utils import render_list


app = Flask(__name__)


@app.route("/")
def main():
    return "hw5"


@app.route("/bitcoin_rate")
def get_bitcoin_rate():

    currency = request.args.get('currency', 'USD')  # Получение параметра currency из запроса (по умолчанию USD)

    data = requests.get("https://bitpay.com/api/rates").json()  # Получение данных из https://bitpay.com/api/rates

    # Выбор записи с соответствующей валютой
    for record in data:
        if record['code'] == currency:
            result = f"{record['code']}: {record['rate']}"
            break
    else:
        result = f"Error. Currency {currency} does not exist."

    # data = list(filter(lambda el: el['code'] == currency, data))

    return result


@app.route("/random_users")
def gen_random_users():

    count = request.args.get('count', '100')  # Получение параметра count из запроса (по умолчанию 100)

    try:
        count = int(count)
    except ValueError:
        return "Error. Count is not integer", 400

    if count <= 0:
        return "Error. Count must be positive", 400

    f = Faker()

    result = "<br>".join([f"<b>{f.name()}</b> {f.email()}" for i in range(count)])

    return result


@app.route("/unique_names")
def get_unique_names():

    result = execute_query("""SELECT COUNT(DISTINCT FirstName) FROM Customers;""")[0]

    return render_list(result)


@app.route("/tracks_count")
def get_tracks_count():

    result = execute_query("""SELECT COUNT(*) FROM Tracks;""")[0]

    return render_list(result)


@app.route("/customers")
def get_customers():

    city = request.args.get('city')  # Получение параметра city из запроса (по умолчанию None)
    country = request.args.get('country')  # Получение параметра country из запроса (по умолчанию None)

    query = "SELECT FirstName, LastName, Country, City FROM Customers"  # Если в запросе ничего не указано, выводится все

    if not (city is None and country is None):  # Если в запросе хоть что-то указано (город или страна)
        query += " WHERE "

        if city is not None and country is None:  # Если в запросе указана только страна
            query += f"City = '{city}'"
        elif city is None and country is not None:  # Если в запросе указан только город
            query += f"Country = '{country}'"
        else:
            query += f"City = '{city}' AND Country = '{country}'"  # Если в запросе указан и город и страна

    result = execute_query(query)

    return render_list(result)

    # country = request.args['country']
    # city = request.args['city']
    #
    # result = execute_query("""SELECT FirstName, LastName, Country, City
    #                               FROM Customers
    #                               WHERE Country = ? AND City = ?""", (country, city))
    #
    # return render_list(result)


app.run()
